---
title: plus de 2500 fiches complètes
image: <i class="material-icons md-48 wow bounceIn text-primary">trending_up</i>
---
La base contient plus de 2500 fiche complètes (faune, flore) y compris des mamifères marins et oiseaux.
De nombreuse fiches sont en cours de préparation (pour un total de plus de 4500 fiches).