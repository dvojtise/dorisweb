---
title: 100% bénévol et gratuit
image: <i class="material-icons md-48 wow bounceIn text-primary">people_outline</i>
---
Tout comme le site DORIS, l'application est développée dans un esprit de travail collectif, elle est, et restera, gratuite.
Le contenu des fiches est réalisé par des participants bénévoles, amateurs et/ou scientifiques, dans le cadre de la fédération française de plongée (FFESSM) et de sa commission de biologie (CNEBS). 
 