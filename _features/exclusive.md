---
title: Quelques fonctions exclusives
image: <i class="material-icons md-48 wow bounceIn text-primary">highlight</i>
---
L'application propose même quelques fonctionnalités exclusives (non disponibles sur le site web http://doris.ffessm.fr). Comme par exemple:
la navigation directe depuis les fiches vers les définitions du glossaire, le filtrage immédiat dans les listes, ou la recherche par image (scroll rapide sur toutes les images, y compris les images secondaires) 